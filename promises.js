// we learn in later

// const myPromise = new Promise((resolve) => resolve("Hello World"));
let data = false;
const fs = require("fs");

const getListMovies = () => {
  const myPromise = new Promise((resolve, reject) => {
    fs.readFile("./movies.json", "utf-8", (err, data) => {
      if (err) {
        reject("Error datanya");
      } else {
        resolve(JSON.parse(data));
      }
    });
    //   if (data) {
    //     resolve("data benar");
    //   } else {
    //     reject("data salah");
    //   }
  });
  return myPromise;
};

// Karena tidak ada reject, maka kita tidak perlu menulis .catch
// myPromise
//   .then((data) => {
//     console.log(data);
//   })
//   .catch((e) => {
//     console.log("e", e);
//   });

const createNewData = (movie) => {
  const myPromise = new Promise((resolve, reject) => {
    fs.writeFile("./movies.json", movie, (err) => {
      if (err) {
        reject("Error datanya");
      } else {
        resolve(JSON.parse(data));
      }
    });
  });
  return myPromise;
};
const addMovie = () => {
  // console.log("get")
  getListMovies()
    .then((data) => {
        return createNewData()
    })
    .then((databaru) => {
      console.log("data baru", databaru);
    })
    .catch((e) => {
      console.log("error", e);
    });
};

addMovie();
