// proses syncronous

function getCategory() {
  console.log("get category");
}
function getProducts() {
  console.log("get products");
}

function getUserProfile() {
  console.log("get user profile");
}

function checkIsUserBeingLogin(func) {
  let value = false;
  console.log("check if user is login or not");
  if (value === true) {
    func();
  }
  return value;
}

// getCategory(); // 2 sec
// getUserProfile(); // 10 sec - > ke execute 2s
// getProducts(); // 2 sec - > ke execute di 12 s
// output kode akan ditunda selama 100 milliseconds
setTimeout(() => {
  getCategory();
}, 100);

setTimeout(() => {
  getProducts();
}, 500);

// A callback is a function passed as an argument to another function.
setTimeout(() => {
  checkIsUserBeingLogin(getUserProfile);
}, 1000);

document.getElementById("my_button").addEventListener("click", function () {
  alert("Oops aku di klik nih!");
});

document.getElementById("my_button").addEventListener("click");
alert("Oops aku di klik nih!");

// setTimeout(() => {
//   getUserProfile();
// }, 700);

// halo binarian developer javascript
// Hello Binarian! JavaScript Developer
